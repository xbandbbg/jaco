package com.xb.poi;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;

import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.Date;

public class demo4 {

    public static void main(String[] args) throws Exception{

        Workbook wb = new HSSFWorkbook(); //定义一个新的工作薄

        Sheet sheet = wb.createSheet("第一个Sheet页");  //创建第一个sheet页

        Row row = sheet.createRow(0); //创建一个行

        Cell cell = row.createCell(0);  //创建一个单元格 第1列

        cell.setCellValue(new Date()); //给单元格设置值

        CreationHelper creationHelper = wb.getCreationHelper();
        CellStyle cellStyle=wb.createCellStyle();//单元格样式
        cellStyle.setDataFormat(creationHelper.createDataFormat().getFormat("yyyy-mm-dd hh:mm:ss"));
        cell=row.createCell(1);//第二列
        cell.setCellValue(new Date());
        cell.setCellStyle(cellStyle);

        cell=row.createCell(2);//第三列
        cell.setCellValue(Calendar.getInstance());
        cell.setCellStyle(cellStyle);

        FileOutputStream fileOut = new FileOutputStream("D:\\POI\\工作薄.xls");

        wb.write(fileOut);

    }
}
